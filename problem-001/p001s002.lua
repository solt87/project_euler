#! /usr/bin/env lua

--[[

If we list all the natural numbers below 10 that are multiples of
3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

--]]

--[[

Second attempt.

Result seems correct.

1. Build a list of 3-divisible numbers, and
2. another one of 5-divisible numbers (minus those in the first list), and
3. sum those.

--]]

--[[ Constants ]]

MAX = 999    -- The maximum number we want to deal with.


--[[ Variables ]]

threenums = {}    -- list for the numbers divisible by three
fivenums = {}     -- list for the numbers divisible by five

sum = 0


--[[ Helper functions ]]

function summer( list )    -- Sum the elements of 'list', and return the sum
        local accumulator = 0
        
        for i = 1, #list do
                accumulator = accumulator + list[ i ]
        end
        
        return accumulator
end


--[[ Main part ]]

-- Fill the list of 3-divisible numbers within the boundaries

num = 3
i = 1
while num <= MAX do
        threenums[ i ] = num
        num = num + 3
        i = i + 1
end

--[[
for i = 1, #threenums do
        io.write( threenums[ i ] .. ", " )
end
--]]


-- Fill the list of numbers divisible by 5 (but not by 3)
-- within the boundaries.

num = 5
i = 1
while num <= MAX do
        if num % 3 ~= 0 then
                fivenums[ i ] = num
                i = i + 1
        end

        num = num + 5
end

--[[
for i = 1, #fivenums do
        io.write( fivenums[ i ] .. ", " )
end
--]]

-- Sum all the elements of the two lists (I know, they're tables...).

sum = summer( threenums ) + summer( fivenums )

-- Report the result

print( sum )
